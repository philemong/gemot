# GeMot

Ce petit programme est destiné à inventer des mots qui ressemblent à une langue donnée.
Il s'agit du premier programme que j'ai réalisé entièrement, courant 2018.

## Compilation

Le programme est en C++/Qt. Il est prévu pour fonctionner avec Qmake, je recommande QtCreator.

Le dossier "WordLists" contient les paramètres par défaut du programme et des listes de mot français. Ce dossier doit se trouver dans le même répertoire que le ficher executable. Vous pouvez copier le dossier à la main. Si vous souhaitez que cela se fasse automatiquement, il faut ajouter une étape `make install` en dernière étape de build dans QtCreator.

## Fichier de liste de mot

Le programme se base sur une liste de mot, qu'il analyse pour générer des mots qui ressemblent à cette liste.
Le dossier `WordLists`contient par défaut trois liste de mot Français : une très complète, une beaucoup plus courte, et une ne comportant que des mots sans accents.

Vous pouvez générer votre propre liste de mot : pour cela, créer un fichier texte comportant un seul mot par ligne (tout caractère après le premier espace sera ignoré). Il est ensuite possible de selectionner le nouveau fichier créé dans l'onglet `Analyse`.

## Erreurs possibles

### Dossier WordLists

Si au lancement du programme, la barre d'état en bas indique "Problème lors de la lecture des valeurs par défaut", c'est que le fichier `WordList/Parametres.xml` comporte un problème.

Si elle indique "Impossible de lire la liste de mot" c'est que la liste de mot par défaut (`WordList/Mot_FR_full.txt`) a un problème.

La meilleure solution est de s'assurer que le dossier WordLists à bien été copié dans le même dossier que l'executable, ou de l'y copier manuellement.

### Modifications des paramètres

Attention : Losrque l'on lance le programme depuis QtCreator, le fichier `Paramètre.xml` est réécrasé par son modèle présent dans le dossier source. Les modifications faite via `Menu/Changer les paramètre par défaut` sont donc effacées.

### Changer de liste de mot

Il est possible de le programme crash à si vous tentez de charger une liste de mot non valide (càd pas un fichier texte). Je remédierai peut être à ce problème un jour...

### Release / Debug

L'analyse de la liste de mot est nettement plus longue en compilation `Debug` que `Release`

## Remerciement

L'idée du programme vient de David Louapre et de sa chaîne "Science étonnante". Voir [sa vidéo sur le sujet](https://www.youtube.com/watch?v=YsR7r2378j01) pour mieux comprendre cet algorithme.
