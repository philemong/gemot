TEMPLATE = app
CONFIG += qt
CONFIG += c++11

QT += widgets

SOURCES += main.cpp \
    fonctions.cpp \
    fenaide.cpp \
    fenetreprincipale.cpp \
    infos.cpp

HEADERS += \
    fonctions.h \
    fenaide.h \
    fenetreprincipale.h \
    infos.h

DISTFILES +=

FORMS += \
    fenaide.ui \
    fenetreprincipale.ui \
    infos.ui

RESOURCES += \
    ressources.qrc

#Copy Wodrlists folder to compilation folder.
#Attention : pour copier de l'intérieur du fichier PWD/Wordlists, il faut que
#le path de sortie soit OUT_PWD/Wordlists, pas juste OUT_PWD
#A creuser...
wordlists.path = $$OUT_PWD/WordLists
wordlists.files += $$PWD/WordLists/*

INSTALLS += \
    wordlists
